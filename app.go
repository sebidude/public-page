package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	duration, _ := strconv.Atoi(os.Args[1])
	fmt.Println("hello from app")
	time.Sleep(time.Duration(duration) * time.Second)
}
